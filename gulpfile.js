var gulp = require('gulp');
var stylus = require('gulp-stylus');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var babel = require('gulp-babel');

gulp.task('default', ['build-scripts', 'build-style']);

gulp.task('build-scripts', function(){
	return gulp.src('./src/*.jsx')
	.pipe(babel({
		presets: ['env', 'react', 'minify']
	}))
	.pipe(gulp.dest('./dist'))
});

gulp.task('build-style', function(){
	var plugins = [
		autoprefixer()
	];

	return gulp.src('./src/style.styl')
	.pipe(stylus({
		compress: true
	}))
	.pipe(postcss(plugins))
	.pipe(gulp.dest('./dist'))
});