import React, {Component} from 'react';
import PropTypes from 'prop-types';

class ParallaxContent extends Component {
	constructor(){
		super(...arguments);
	}

	render(){
		const className = `parallax-content-holder${this.props.className && " " + this.props.className || ''}`;
		const styles = Object.assign({overflow: 'hidden'}, this.props.styles || {});
		return <div className={className} style={styles}>
			<div className="parallax-content">
				{this.props.children}
			</div>
		</div>
	}
}

ParallaxContent.propTypes = {
	
}

export default ParallaxContent;