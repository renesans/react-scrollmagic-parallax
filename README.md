react-scrollmagic-parallax
===================

```
npm install --save react-scrollmagic-parallax
```

or

```
yarn add react-scrollmagic-parallax
```

### Usage

####Styles

Insert below CSS in your style sheets.

```css
  .parallax .parallax-content-holder {
    position: relative;
    -webkit-perspective: 1000px;
            perspective: 1000px;
    z-index: -1;
  }
  .parallax .parallax-content-holder .parallax-content {
    -webkit-transform: translateZ(0);
            transform: translateZ(0);
    -webkit-backface-visibility: hidden;
            backface-visibility: hidden;
  }
```

or import into bundler

```js
  import 'react-scrollmagic-parallax/dist/style';
```

####Code

```react
  import React, {Component} from 'react';
  import PropTypes from 'prop-types';

  import {Power2} from 'gsap';
  import Parallax, {ParallaxContent} from 'react-scrollmagic-parallax';

  class ParallaxTestComponent extends Component {

      constructor(){
          super(...arguments)
      }

      render(){

          const parallaxProps = {
              //parallax component props

              offset: 10,
              ease: Power2.easeNone,
              power: 0.4,
              container: '.scrollable-container'

              //See below for more info
          };

          <div className="scrollable-container">
              <Parallax {...parallaxProps}>

                  <ParallaxContent>
                      {/*Here put childs (some content) which should be parallaxed (between sections) */}
                  </ParallaxContent>

                  {/*Outside ParallaxContent component can be any content which is not parallaxed (sections)*/}

                  <div style={{padding: '50px', backgroundColor: 'blue'}}>First section content</div>

                  <ParallaxContent>
                      {/* some parallaxed content */}
                  </ParallaxContent>

                  <div style={{padding: '100px', backgroundColor: 'red'}}>Second section content</div>

                  <ParallaxContent className="custom-classname" style={{backgroundColor: blue}}>
                      {/* ParallaxContent component accepts custom class names and additional styles */}
                  </ParallaxContent>

                  {/* and so on... */}

              </Parallax>
          </div>
      }

  }

  ParallaxTestComponent.propTypes = {

  }

  export default ParallaxTestComponent
```

###Properties

See Global Options below to set settings for all parallax component.

Properties affects only one parallax component and overrides global options.

| name | type | description | default |
|---------------|------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------|
| offset | number | Sets parallax element offset from viewport top in pixels. E.g. for main navigation bar | 0 |
| power | number (0.0 ... 1.0) | Parallax effect strength. Number from 0 to 1 indicates percent of ParallaxContent component height | 0.2 |
| ease | object or Gsap ease function | Any of gsap animation ease functions (except Custom). Or object which contains 'enter' and 'exit' properties with ease functions assignment: ease = {       enter: Power2.easeIn,       exit: Power2.easeOut } | No ease (Linear) |
| container | DOMElement or string | Scrollable DOM element (container) or selector.  Also you can simply put class name 'parallax-scroll' to any scrollable component. | '.parallax-scroll' |
| disable | boolean or function | Pass true to disable parallax effect. Use function (which returns boolean value)  to do it dynamically | false |
| disableMobile | boolean, function or number | Pass true to disable parallax effect on mobiles, because some devices has performance issues. Device is recognized as mobile and parallax effect is disabled when viewport width is less then 600px. To change viewport width breakpoint, pass number to this prop. For dynamic disabling, use function which returns boolean or number value. | false |
| id | string | Assign id | undefined |
| className | string | Additional class names | undefined |
| style | object | Additional styles | undefined |

###Global Options

```js
  import Parallax from 'react-scrollmagic-parallax';
    
    Parallax.setGlobalOptions({
      //properties. See above
    });       
```

Here is additional property for global options

| name | type | description | default |
|--------------------|--------|---------------------------------------------------|---------|
| defaultMobileWidth | number | Overrides default 600px viewport width breakpoint for  | 600 |


###Author

Edvinas pranka

https://www.ceonnel.lt

# License

The MIT License (MIT)

Copyright (c) 2017 Edvinas Pranka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.